import React, { useState, useEffect } from 'react'

function Sw(props) {

    const [status, setStatus] = useState('idle');
    const [query, setQuery] = useState(props.next);
    const [data, setData] = useState([]);
    const [people, setPeople] = useState(props.people);
    

    const myFetch = () => {
        if (status !== 'idle') return;
        const fetchData = async () => {
            console.log('Fetching...');
            setStatus('fetching');
            const response = await fetch(
                query
            );
            const data = await response.json();
            setData(data);
            let myCharacters = [];
            for (const character of data.results) {
                myCharacters = [...myCharacters, { name: character.name, height: character.height, weight: character.weight }];
            }
            setPeople([...people, ...myCharacters]);
            props.addPeople([...people, ...myCharacters], data.next);
            setStatus('fetched');
        };

        fetchData();

        // ComponentWillUnmount
        return () => {
            console.log('SW will unmount');
        }
    };


    useEffect(myFetch, [query]);

    const moreCharacters = () => {
        setQuery(data.next);
    }

    let content = <div></div>

    let elements = people.map((character, index) =>
        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-4" key={index}>
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">{character.name}</h4>
                    <p class="card-text">{character.height}</p>
                    <p class="card-text">{character.weight}</p>
                </div>
            </div>
        </div>
    );

    if (people.length > 0) {
        content = <div class="row">
            {elements}
        </div>
    } else {
        content = <h1>{status}</h1>
    }

    return (
        <div class="container">
            {content}

            <button type="button" class="btn btn-primary mt-3" onClick={moreCharacters} disabled={data.next===null}>Más personajes</button>
        </div>
    )
}

export default Sw
