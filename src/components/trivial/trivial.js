import React, { useState, useEffect } from 'react'
import Card from '../../model/card';
import Spinner from 'react-bootstrap/Spinner';
import TrivialCard from './trivialcard';

function Trivial(props) {

    const [data, setData] = useState({});
    const [cards, setCards] = useState(props.cards);
    const [points, setPoints] = useState(props.points);
    const [answeredCards, setAnsweredCards] = useState(0);

    useEffect(() => {
        if(answeredCards === cards.length) {
            doRequest();
        }
    }, [answeredCards])

    const doRequest = (amount) => {
        let url = 'https://opentdb.com/api.php';
        if (amount !== undefined && typeof amount === 'number') {
            url = url + '?amount=' + amount;
        } else {
            url = url + '?amount=10';
        }
        console.log(url);
        fetch(url)
            .then(response => response.json())
            .then(jsonData => {
                console.log(JSON.stringify(jsonData));
                setData(data);
                let tempCards = [];
                for (const jsonCard of jsonData.results) {
                    tempCards = [...tempCards, new Card(jsonCard)]
                }
                setCards([...cards, ...tempCards]);
                props.updateCards([...cards, ...tempCards]);
            })
    }

    useEffect(doRequest, [])

    const handleAnswer = (rightAnswered, myCard, index) => {
        console.log('Index: ' + index);
        setAnsweredCards(answeredCards + 1);
        if (rightAnswered) {
            setPoints(points + 2)
            props.updatePoints(points + 2);
        } else {
            setPoints(points - 1)            
            props.updatePoints(points - 1);
        }
        console.log('myCard: ' + JSON.stringify(myCard));
        let tmp = props.cards;
        tmp[index] = myCard;
        props.updateCards(tmp);
    }

    let content =
        <Spinner animation="border" role="status">
            <span className="sr-only">Loading...</span>
        </Spinner>;

    if (cards.length > 0) {

        content =
            <div className="row">
                {cards.map((card, index) =>
                    <div className="col-xs-12 col-sm-12 col-md-6 col-lg-4" key={index}>
                        <TrivialCard card={card} correctAnswer={(e,a)=>{handleAnswer(e,a, index)}}/>
                    </div>
                )}
            </div>

    }
    return (
        <div className="container">
            <h1>
                <h1-6>Puntos: <span class="badge badge-primary">{points}</span></h1-6>
            </h1>
            {content}
        </div>
    )
}

export default Trivial
