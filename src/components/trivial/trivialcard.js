import React, {useState, useEffect} from 'react'

function TrivialCard({ card, correctAnswer }) {
    let buttons = <div></div>;
    const [myCard, setMyCard] = useState(card);
    const [answered, setAnswered] = useState(false);

    let handleClick = (answer, index) => {
        let tmp = myCard;
        tmp.answered = true;
        tmp.answeredIndex = index;
        tmp.userAnswer = answer;
        setAnswered(true);
        card = tmp;
        setMyCard(tmp);
        if(answer === myCard.correctAnswer) {
            correctAnswer(true, myCard);
        }else {
            correctAnswer(false, myCard);
        }
    }

    const getClassButton = (index, answer) => {
        if(myCard.answered === false) {
            return "btn btn-primary btn-lg btn-block"
        }else {
            if(answer === myCard.correctAnswer) {
                return "btn btn-success btn-lg btn-block"
            } else if(index === myCard.answeredIndex) {
                return "btn btn-danger btn-lg btn-block"
            } else {
                return "btn btn-secondary btn-lg btn-block"
            }
        }
    }

    if (myCard.answers.length > 0) {
        buttons = myCard.answers.map((answer, index) => {
            console.log(myCard.answered);
            if (myCard.answered) {
                return <button type="button" name="" id="" className={getClassButton(index, answer)} disabled key={index} dangerouslySetInnerHTML={{__html:answer}} ></button>;
            } else {
                return <button type="button" name="" id="" className={getClassButton(index, answer)} key={index} onClick={() => { handleClick(answer, index) }} dangerouslySetInnerHTML={{__html:answer}}></button>;
            }
        }

        );
    }

    const question = myCard.question;

    return (
        <div className="card">
            <div className="card-body">
                <h4 className="card-title" dangerouslySetInnerHTML={{__html:question}}></h4>
                {buttons}
            </div>
        </div>
    )
}

export default TrivialCard
