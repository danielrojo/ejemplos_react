import React, { useState } from 'react';
import BeerRange from './beerrange';
import BeersList from './beerslist';


function Beers({range, updateRange}) {

    const [value, setValue] = useState(range);

    const update = (r) => {
        updateRange(r);
        setValue(r);
    }

    return (
        <div class="container">
            <BeerRange range={(range)=>{update(range)}} myRange={range}/>
            <BeersList range={value}/>
        </div>
    )
}

export default Beers