import React, {useState, useEffect} from 'react'

function BeersList(props) {

    const [status, setStatus] = useState('idle');
    const [query, setQuery] = useState('https://api.punkapi.com/v2/beers');
    const [data, setData] = useState([]);
    const [beers, setBeers] = useState([]);

    const myFetch = () => {
        if (status !== 'idle') return;

        const fetchData = async () => {
            console.log('Fetching...');
            setStatus('fetching');
            const response = await fetch(
                query
            );
            const data = await response.json();
            setData(data);
            let myBeers = [];
            for (const beer of data) {
                myBeers = [...myBeers, { name: beer.name, tagline: beer.tagline, image_url: beer.image_url, abv: beer.abv }];
            }
            setBeers(myBeers);
            setStatus('fetched');
        };

        fetchData();
    };

    useEffect(myFetch, []);

    const beersCards =
        beers
            .filter((beer) => beer.abv >= props.range[0] && beer.abv <= props.range[1])
            .map((beer, index) =>
                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-4" key={index}>
                    <div class="card">
                        <img class="card-img-top d-block mx-auto mt-2" src={beer.image_url} style={{ width: '50px' }} alt="" />
                        <div class="card-body">
                            <h4 class="card-title">{beer.name}</h4>
                            <p class="card-text">{beer.tagline}</p>
                            <p class="card-text">{beer.abv}%</p>
                        </div>
                    </div>
                </div>
            );

    return (
        <div class="row mt-4">
            {beersCards}
        </div>
    )
}

export default BeersList
