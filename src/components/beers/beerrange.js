import React, {useState} from 'react';
import Slider from '@material-ui/core/Slider';

function BeerRange(props) {
    const [value, setValue] = useState(props.myRange);

    const handleChange = (event, newValue) => {
        setValue(newValue);
        props.range(newValue);
    };

    return (
        <Slider
            min={0}
            step={0.1}
            max={60}
            value={value}
            onChange={handleChange}
            valueLabelDisplay="auto"
            aria-labelledby="range-slider"
        />
    )
}

export default BeerRange
