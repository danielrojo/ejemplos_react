import React, { Component, Fragment } from 'react'

export class Keyboard extends Component {

    constructor(props) {
        super(props)

        this.state = {

        }
    }

    handleClick(value) {
        this.props.signal(value);
    }

    render() {
        return (
            <Fragment>
                    <tr>
                        <td><button className="btn btn-primary btn-block" type="button" value="1" onClick={() => { this.handleClick(1) }}>1</button> </td>
                        <td><button className="btn btn-primary btn-block" type="button" value="2" onClick={() => { this.handleClick(2) }}>2</button> </td>
                        <td><button className="btn btn-primary btn-block" type="button" value="3" onClick={() => { this.handleClick(3) }}>3</button> </td>
                        <td><button className="btn btn-primary btn-block" type="button" value="/" onClick={() => { this.handleClick('/') }}>/</button> </td>
                    </tr>
                    <tr>
                        <td><button className="btn btn-primary btn-block" type="button" value="4" onClick={() => { this.handleClick(4) }}>4</button> </td>
                        <td><button className="btn btn-primary btn-block" type="button" value="5" onClick={() => { this.handleClick(5) }}>5</button> </td>
                        <td><button className="btn btn-primary btn-block" type="button" value="6" onClick={() => { this.handleClick(6) }}>6</button> </td>
                        <td><button className="btn btn-primary btn-block" type="button" value="-" onClick={() => { this.handleClick('-') }}>-</button> </td>
                    </tr>
                    <tr>
                        <td><button className="btn btn-primary btn-block" type="button" value="7" onClick={() => { this.handleClick(7) }}>7</button> </td>
                        <td><button className="btn btn-primary btn-block" type="button" value="8" onClick={() => { this.handleClick(8) }}>8</button> </td>
                        <td><button className="btn btn-primary btn-block" type="button" value="9" onClick={() => { this.handleClick(9) }}>9</button> </td>
                        <td><button className="btn btn-primary btn-block" type="button" value="+" onClick={() => { this.handleClick('+') }}>+</button> </td>
                    </tr>
                    <tr>
                        <td><button className="btn btn-primary btn-block" type="button" value="." onClick={() => { this.handleClick('.') }}>.</button></td>
                        <td><button className="btn btn-primary btn-block" type="button" value="0" onClick={() => { this.handleClick(0) }}>0</button> </td>
                        <td><button className="btn btn-primary btn-block" type="button" value="=" onClick={() => { this.handleClick('=') }}>=</button></td>
                        <td><button className="btn btn-primary btn-block" type="button" value="*" onClick={() => { this.handleClick('x') }}>x</button> </td>
                    </tr>
            </Fragment >
        )
    }
}

export default Keyboard

