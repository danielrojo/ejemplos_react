import React, { Component } from 'react'
import Display from './display';
import Keyboard from './keyboard';

const INIT_STATE = 0;
const FIRST_FIGURE_STATE = 1;
const SECOND_FIGURE_STATE = 2;
const RESULT_STATE = 3;

export class Calculator extends Component {


    constructor(props) {
        super(props)

        this.calculatorState = this.props.data.calculatorState;
        this.firstFigure = this.props.data.firstFigure;
        this.secondFigure = this.props.data.secondFigure;
        this.result = this.props.data.result;
        this.operator = this.props.data.operator;
    
        this.state = {
            display: this.props.data.display
        }
    }

    componentWillUnmount() {
        console.log('Calculator Will Unmount');
        this.props.updateCalculator({
            calculatorState: this.calculatorState,
            firstFigure: this.firstFigure,
            secondFigure: this.secondFigure,
            result: this.result,
            operator: this.operator,
            display: this.state.display
        });
    }
    

    handleClick(value) {
        this.setState({ display: this.state.display + String(value) });
        if (typeof value === 'number') {
            this.handleNumber(value);
        } else if (typeof value === 'string') {
            this.handleSymbol(value);
        }
    }

    handleNumber(number) {
        switch (this.calculatorState) {
            case INIT_STATE:
                this.firstFigure = number;
                this.calculatorState = FIRST_FIGURE_STATE;
                this.setState({ display: this.state.display + String(number) });
                break;
            case FIRST_FIGURE_STATE:
                this.firstFigure = this.firstFigure * 10 + number;
                this.setState({ display: this.state.display + String(number) });
                break;
            case SECOND_FIGURE_STATE:
                this.secondFigure = this.secondFigure * 10 + number;
                this.setState({ display: this.state.display + String(number) });
                break;
            case RESULT_STATE:
                this.firstFigure = number;
                this.secondFigure = 0;
                this.result = 0;
                this.operator = '';
                this.calculatorState = FIRST_FIGURE_STATE;
                this.setState({ display: String(number) });
                break;
            default:
                break;
        }
    }

    resolve() {
        switch (this.operator) {
            case '+':
                return this.firstFigure + this.secondFigure;
            case '-':
                return this.firstFigure - this.secondFigure;
            case 'x':
                return this.firstFigure * this.secondFigure;
            case '/':
                return this.firstFigure / this.secondFigure;
            default:
                break;
        }
    }

    handleSymbol(symbol) {
        switch (this.calculatorState) {
            case INIT_STATE:
                break;
            case FIRST_FIGURE_STATE:
                if (symbol === '+' || symbol === '-' || symbol === 'x' || symbol === '/') {
                    this.operator = symbol;
                    this.calculatorState = SECOND_FIGURE_STATE;
                    this.setState({ display: this.state.display + symbol });
                }
                break;
            case SECOND_FIGURE_STATE:
                if (symbol === '=') {
                    this.result = this.resolve();
                    this.calculatorState = RESULT_STATE;
                    this.setState({ display: this.state.display + symbol + String(this.result) });
                }
                break;
            case RESULT_STATE:
                if (symbol === '+' || symbol === '-' || symbol === 'x' || symbol === '/') {
                    this.firstFigure = this.result;
                    this.secondFigure = 0;
                    this.result = 0;
                    this.operator = symbol;
                    this.calculatorState = SECOND_FIGURE_STATE;
                    this.setState({ display: String(this.firstFigure) + symbol });
                }
                break;
            default:
                break;
        }
    }

    render() {
        return (
            <div className="container">
                <table border="1">
                    <tbody>
                        <tr>
                            <td colSpan="3">
                                <Display input={this.state.display} />
                            </td>
                            <td><button type="button" className="btn btn-primary" disabled>c</button></td>
                        </tr>
                        <Keyboard signal={(v) => { this.handleClick(v) }} />
                    </tbody>
                </table>
            </div>

        )
    }
}

export default Calculator