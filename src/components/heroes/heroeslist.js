import React, { Component } from 'react'

export class HeroesList extends Component {

    constructor(props) {
        super(props)

        this.state = {

        }
    }

    handleRemove(index) {
        this.props.remove(index);
    }

    render() {
        const heroList = this.props.heroes.map((hero, index) =>
            <li className="list-group-item" key={index}>
                <div>
                <strong>{hero.name}</strong>
                <br />
                <p>{hero.description}</p>
                </div>
                <button type="button" class="btn btn-danger" onClick={()=>{this.handleRemove(index)}}>Eliminar</button>
            </li>
        );

        return (
            <ul className="list-group">
                {heroList}
            </ul>
        )
    }
}

export default HeroesList
