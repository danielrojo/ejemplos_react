import React, { Component } from 'react'
import HeroesList from './heroeslist';
import HeroForm from './heroform';

export class Heroes extends Component {

    constructor(props) {
        super(props)

        this.state = {
            heroes: this.props.heroes
        }
    }

    handleNewHero(hero) {
        this.props.updateHeroes([...this.state.heroes, hero]);
        this.props.updateNewHero({name:'', description:''})
        this.setState({heroes: [...this.state.heroes, hero]})
    }

    handleRemove(index) {
        let tmp = this.props.heroes;
        tmp.splice(index,1);
        this.setState({heroes: tmp});
    }

    render() {

        return (
            <div className="container">
                <HeroForm addHero={(hero)=>{this.handleNewHero(hero)}} newHero={this.props.newHero}/>
                <hr />
                <HeroesList heroes={this.state.heroes} remove={(i)=>{this.handleRemove(i)}}/>
            </div>
        )
    }
}

export default Heroes
