import React, { Component, Fragment } from 'react'

export class HeroForm extends Component {

    constructor(props) {
        super(props)

        this.state = {
            newHero: this.props.newHero
        }
    }

    handleClick() {
        this.props.addHero(this.state.newHero);
        this.setState({ newHero: { name: '', description: '' } });
    }

    handleChange(e) {
        let temp = this.state.newHero;
        if (e.target.name === 'name') {
            temp.name = e.target.value;
        } else if (e.target.name === 'description') {
            temp.description = e.target.value;
        }

        this.setState({ newHero: temp })
    }

    render() {
        return (
            <Fragment>
                <div className="form-group">
                    <label>Nombre</label>
                    <input type="text" name="name" id="" className="form-control" placeholder="" aria-describedby="helpId"
                        value={this.state.newHero.name} onChange={(event) => { this.handleChange(event) }} />
                    <label>Descripción</label>
                    <input type="text" name="description" id="" className="form-control" placeholder="" aria-describedby="helpId"
                        value={this.state.newHero.description} onChange={(event) => { this.handleChange(event) }} />
                </div>
                <button type="button" class="btn btn-primary mt-3" onClick={() => { this.handleClick() }}>Añadir héroe</button>
            </Fragment>
        )
    }
}

export default HeroForm
