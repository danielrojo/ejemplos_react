import './App.css';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";

// Components

// Containers
import Apod from './containers/apodcontainer';
import Heroes from './containers/heroescontainer';
import Calculator from './containers/calculatorcontainer'
import Sw from './containers/swcontainer'
import Beers from './containers/beerscontainer';
import Trivial from './containers/trivialcontainer';

function App() {
  return (
    <Router>
      <div>
        <nav class="navbar navbar-expand-sm navbar-dark bg-primary mb-2">
          <a class="navbar-brand" href="#">Navbar</a>
          <button class="navbar-toggler d-lg-none" type="button" data-toggle="collapse" data-target="#collapsibleNavId" aria-controls="collapsibleNavId"
            aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
          </button>
          <div class="collapse navbar-collapse" id="collapsibleNavId">
            <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
              <li class="nav-item">
                <Link to="/calculator" class="nav-link">Calculadora</Link>
              </li>
              <li class="nav-item">
                <Link class="nav-link" to="/heroes">Héroes</Link>
              </li>
              <li class="nav-item">
                <Link class="nav-link" to="/apod">Apod</Link>
              </li>
              <li class="nav-item">
                <Link class="nav-link" to="/beers">Cervezas</Link>
              </li>
              <li class="nav-item">
                <Link class="nav-link" to="/sw">Star Wars</Link>
              </li>
              <li class="nav-item">
                <Link class="nav-link" to="/trivial">Trivial</Link>
              </li>

            </ul>
          </div>
        </nav>
        
        {/* A <Switch> looks through its children <Route>s and
            renders the first one that matches the current URL. */}
        <Switch>
          <Route path="/calculator">
            <Calculator />
          </Route>
          <Route path="/heroes">
            <Heroes />
          </Route>
          <Route path="/apod">
            <Apod />
          </Route>
          <Route path="/beers">
            <Beers />
          </Route>
          <Route path="/sw">
            <Sw />
          </Route>
          <Route path="/trivial">
            <Trivial/>
          </Route>
          <Route path="*">
            <h1>Esta ruta no existe</h1>
          </Route>
        </Switch>
      </div>
    </Router>
    // <div className="container">
    //   <Tabs defaultActiveKey="sw" id="uncontrolled-tab-example">
    //     <Tab eventKey="calculator" title="Calculadora">
    //       <Calculator />
    //     </Tab>
    //     <Tab eventKey="heroes" title="Héroes">
    //       <Heroes />
    //     </Tab>
    //     <Tab eventKey="apod" title="Apod">
    //       <Apod/>
    //     </Tab>
    //     <Tab eventKey="beers" title="Cervezas">
    //       <Beers/>
    //     </Tab>
    //     <Tab eventKey="sw" title="Star Wars">
    //       <Sw/>
    //     </Tab>
    //   </Tabs>
    // </div>
  );
}

export default App;
