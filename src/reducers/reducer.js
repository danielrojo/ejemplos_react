const reducer = (state = {
    sw: {
        people: [],
        next: 'https://swapi.dev/api/people/'
    },
    beers: {
        range: [0, 6]
    },
    calculator: {
        calculatorData: {
            calculatorState: 0,
            firstFigure: 0,
            secondFigure: 0,
            result: 0,
            operator: '',
            display: ''
        }
    },
    heroes: {
        heroes: [
            { name: 'Batman', description: 'dark knight' },
            { name: 'Spiderman', description: 'Spidy' }
        ],
        newHero: { name: '', description: '' }
    },
    apod: {
        dates: [],
        data: []
    },
    trivial: {
        cards: [],
        points: 0
    }
}, action) => {
    switch (action.type) {
        case "ADD_PEOPLE":
            state.sw.people = action.payload.swPeople;
            state.sw.next = action.payload.next;
            return state;
        case "UPDATE_RANGE":
            state.beers.range = action.payload.range;
            return state;
        case "UPDATE_CALCULATOR":
            state.calculator.calculatorData = action.payload.data;
            return state;
        case "UPDATE_NEW_HERO":
            state.heroes.newHero = action.payload.newHero;
            return state;
        case "UPDATE_HERO_LIST":
            state.heroes.heroes = action.payload.heroes;
            return state;
        case "UPDATE_DATES":
            state.apod = action.payload;
            return state;
        case "UPDATE_POINTS":
            state.trivial.points = action.payload.points;
            return state;
        case "UPDATE_CARDS":
            state.trivial.cards = action.payload.cards;
            return state;

        default:
            return state;
    }
};

export default reducer;