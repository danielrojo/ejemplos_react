import Beers from "../components/beers/beers";
import { connect } from "react-redux";

const mapStateToProps = state => {
  return {
    range: state.beers.range,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    updateRange: (range) => {
      dispatch({ type: "UPDATE_RANGE", payload: { range: range } });
    }
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Beers);