import Apod from "../components/apod/apod";
import { connect } from "react-redux";

const mapStateToProps = state => {
  return {
    dates: state.apod.dates,
    data: state.apod.data
  };
};

const mapDispatchToProps = dispatch => {
  return {
    updateDates: (data, dates) => {
      dispatch({ type: "UPDATE_DATES", payload: { dates: dates, data: data } });
    }
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Apod);