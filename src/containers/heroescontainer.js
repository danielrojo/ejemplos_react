import Heroes from "../components/heroes/heroes";
import { connect } from "react-redux";

const mapStateToProps = state => {
  return {
    newHero: state.heroes.newHero,
    heroes: state.heroes.heroes
  };
};

const mapDispatchToProps = dispatch => {
  return {
    updateNewHero: (hero) => {
      dispatch({ type: "UPDATE_NEW_HERO", payload: { newHero: hero } });
    },
    updateHeroes: (heroes) => {
      dispatch({ type: "UPDATE_HERO_LIST", payload: { heroes: heroes } });
    }
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Heroes);