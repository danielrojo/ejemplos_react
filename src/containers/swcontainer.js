import Sw from "../components/sw/sw";
import { connect } from "react-redux";

const mapStateToProps = state => {
  return {
    people: state.sw.people,
    next: state.sw.next
  };
};

const mapDispatchToProps = dispatch => {
  return {
    addPeople: (people, next) => {
      dispatch({ type: "ADD_PEOPLE", payload: { swPeople: people, next: next } });
    }
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Sw);