import Trivial from "../components/trivial/trivial";
import { connect } from "react-redux";

const mapStateToProps = state => {
  return {
    cards: state.trivial.cards,
    points: state.trivial.points
  };
};

const mapDispatchToProps = dispatch => {
  return {
    updatePoints: (points) => {
      dispatch({ type: "UPDATE_POINTS", payload: { points } });
    },
    updateCards: (cards) => {
      dispatch({ type: "UPDATE_CARDS", payload: { cards } });
    }
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Trivial);