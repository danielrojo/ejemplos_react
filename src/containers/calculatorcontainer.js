import Calculator from "../components/calculator/calculator";
import { connect } from "react-redux";

const mapStateToProps = state => {
  return {
    data: state.calculator.calculatorData,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    updateCalculator: (myData) => {
      dispatch({ type: "UPDATE_CALCULATOR", payload: { data: myData } });
    }
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Calculator);