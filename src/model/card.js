export class Card {

    constructor(json) {
        this.question = json.question;
        this.category = json.category;
        this.answers = json.incorrect_answers;
        this.answers = [... this.answers, json.correct_answer];
        this.shuffleArray()
        this.correctAnswer = json.correct_answer;
        this.answered = false;
        this.answeredIndex = -1;
        this.userAnswer = '';

    }

    shuffleArray() {
        for (var i = this.answers.length - 1; i > 0; i--) {
            var j = Math.floor(Math.random() * (i + 1));
            var temp = this.answers[i];
            this.answers[i] = this.answers[j];
            this.answers[j] = temp;
        }
    }
}

export default Card